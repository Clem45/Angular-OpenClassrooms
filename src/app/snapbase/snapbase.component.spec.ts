import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SnapbaseComponent } from './snapbase.component';

describe('SnapbaseComponent', () => {
  let component: SnapbaseComponent;
  let fixture: ComponentFixture<SnapbaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SnapbaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SnapbaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
