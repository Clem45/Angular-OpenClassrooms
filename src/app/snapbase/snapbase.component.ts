import { Component, OnInit } from '@angular/core';
import { biscuit } from '../models/biscuit.model';
import { SnapserviceService } from '../snapservice.service';
@Component({
  selector: 'app-snapbase',
  templateUrl: './snapbase.component.html',
  styleUrls: ['./snapbase.component.scss']
})
export class SnapbaseComponent implements OnInit {
  biscuitarray!: biscuit[];
  constructor(private snapserv: SnapserviceService) {}

  ngOnInit(): void {
    
  this.biscuitarray = this.snapserv.getsnaps();
  }

}
