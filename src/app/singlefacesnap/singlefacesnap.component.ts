import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { biscuit } from '../models/biscuit.model';
import { SnapserviceService } from '../snapservice.service';

@Component({
  selector: 'app-singlefacesnap',
  templateUrl: './singlefacesnap.component.html',
  styleUrls: ['./singlefacesnap.component.scss']
})
export class SinglefacesnapComponent implements OnInit {
  constructor(private faceSnapsService: SnapserviceService,
              private route: ActivatedRoute ) {}
  @Input() biscuit !:biscuit
  buttonText!: string;

  onSnap() {
    if (this.buttonText === 'Oh Boy') {
      this.faceSnapsService.snapFaceSnapById(this.biscuit.id, 'snap');
      this.buttonText = 'STOP';
    } else {
      this.faceSnapsService.snapFaceSnapById(this.biscuit.id, 'unsnap');
      this.buttonText = 'Oh Boy'
    }
  }
  
  ngOnInit() {
      this.buttonText = 'Oh Boy'
      const facesnapId = +this.route.snapshot.params['id'];
      this.biscuit = this.faceSnapsService.getfacesnapbyid(facesnapId);
  }
}
