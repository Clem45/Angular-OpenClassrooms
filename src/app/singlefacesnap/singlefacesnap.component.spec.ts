import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglefacesnapComponent } from './singlefacesnap.component';

describe('SinglefacesnapComponent', () => {
  let component: SinglefacesnapComponent;
  let fixture: ComponentFixture<SinglefacesnapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SinglefacesnapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglefacesnapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
