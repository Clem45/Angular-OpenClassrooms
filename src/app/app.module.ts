import { registerLocaleData } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FacesnapComponent } from './facesnap/facesnap.component';
import * as fr from '@angular/common/locales/fr';
import { HeaderComponent } from './header/header.component';
import { SnapbaseComponent } from './snapbase/snapbase.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { SinglefacesnapComponent } from './singlefacesnap/singlefacesnap.component'
@NgModule({
  declarations: [
    AppComponent,
    FacesnapComponent,
    HeaderComponent,
    SnapbaseComponent,
    LandingpageComponent,
    SinglefacesnapComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue:'fr-FR'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(){
    registerLocaleData(fr.default);
  }
 }
