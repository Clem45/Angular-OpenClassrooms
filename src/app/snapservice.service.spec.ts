import { TestBed } from '@angular/core/testing';

import { SnapserviceService } from './snapservice.service';

describe('SnapserviceService', () => {
  let service: SnapserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SnapserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
