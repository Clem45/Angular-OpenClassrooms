import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FacesnapComponent } from './facesnap/facesnap.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { SinglefacesnapComponent } from './singlefacesnap/singlefacesnap.component';
import { SnapbaseComponent } from './snapbase/snapbase.component';

const routes: Routes = [
  {path: 'facesnaps/:id', component: SinglefacesnapComponent},
  {path: 'facesnaps', component: SnapbaseComponent},
  {path: '', component:LandingpageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
