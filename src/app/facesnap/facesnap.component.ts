import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { biscuit } from '../models/biscuit.model';
import { SnapserviceService } from '../snapservice.service';

@Component({
  selector: 'app-facesnap',
  templateUrl: './facesnap.component.html',
  styleUrls: ['./facesnap.component.scss']
})
export class FacesnapComponent implements OnInit {
  constructor(private faceSnapsService: SnapserviceService,
              private router : Router) {}
  @Input() biscuit !:biscuit
  buttonText!: string;

  onSnap() {
    if (this.buttonText === 'Oh Boy') {
      this.faceSnapsService.snapFaceSnapById(this.biscuit.id, 'snap');
      this.buttonText = 'STOP';
    } else {
      this.faceSnapsService.snapFaceSnapById(this.biscuit.id, 'unsnap');
      this.buttonText = 'Oh Boy'
    }
  }
  
  ngOnInit() {
      this.buttonText = 'Oh Boy'
  }
  onviewfacesnap(){
    this.router.navigateByUrl(`facesnaps/${this.biscuit.id}`)
  }
}
