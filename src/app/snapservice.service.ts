import { Injectable } from '@angular/core';
import { biscuit } from './models/biscuit.model';

@Injectable({
  providedIn: 'root'
})
export class SnapserviceService {
  biscuitarray: biscuit[] = [
    {
      id: 1,
      title: 'Un bon repas',
      description: 'Mmmh que c\'est bon !',
      imageUrl: 'https://i.redd.it/xmwzcwvzswh81.jpg',
      createdDate: new Date(),
      snaps: 100
    },
    {
      id:2,
      title: 'Archibad',
      description: 'Mon meilleur ami depuis tout petit !',
      imageUrl: 'https://cdn.pixabay.com/photo/2015/05/31/16/03/teddy-bear-792273_1280.jpg',
      createdDate: new Date(),
      snaps: 4
    },
    {
      id:3,
      title: 'Three Rock Mountain',
      description: 'Un endroit magnifique pour les randonnées.',
      imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Three_Rock_Mountain_Southern_Tor.jpg/2880px-Three_Rock_Mountain_Southern_Tor.jpg',
      createdDate: new Date(),
      snaps: 2
    },
    {
      id:4,
      title: 'Un bon repas',
      description: 'Mmmh que c\'est bon !',
      imageUrl: 'https://wtop.com/wp-content/uploads/2020/06/HEALTHYFRESH.jpg',
      createdDate: new Date(),
      location : 'Paris',
      snaps: 1000
    }

  ];
  getsnaps(): biscuit[]{
    return this.biscuitarray
  }
  getfacesnapbyid(faceSnapId: number) {
    const faceSnap = this.biscuitarray.find(faceSnap => faceSnap.id === faceSnapId)
    if(!faceSnap){
      throw new Error('Facesnap not found');
    }else{
      return faceSnap;
    }
  }

  snapFaceSnapById(faceSnapId: number, snapType: 'snap' | 'unsnap'): void {
    const faceSnap = this.getfacesnapbyid(faceSnapId);
    snapType === 'snap' ? faceSnap.snaps++ : faceSnap.snaps--;
}
}
